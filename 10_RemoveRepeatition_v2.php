<?php
/**
 * 问题：对数组去重并从大到小排序，要求运行时间不超过1秒
 *
 * 第二种思路：先从小到大进行排序然后去重
 * 排序的话可以使用冒泡
 *
 * 由于排序之后相同的数会紧挨在一起，所以只需要在输出的时候判断一下当前的数a[i]与前面的数a[i-1]是否相同，相同的话则说明这个数已经输出了不用再次输出
 */

// 需排序去重的数组
$array = [20, 40, 32, 67, 40, 20, 89, 300, 400, 15];

// 进行冒泡排序
$array = BubbleSort($array);

// 先存第一个数
$result[] = $array[0];

$count = count($array);
// 如果和前一个数相同的话则不输出，否则输出
for ($i = 1; $i < $count; $i++) {
    if ($array[$i] != $array[$i - 1]) {
        $result[] = $array[$i];
    }
}

print_r($result);

/**
 * 冒泡排序
 * @param $array array
 * @return array
 */
function BubbleSort($array)
{
    $num = count($array);

    for ($i = 1; $i < $num; $i++) {
        for ($j = 0; $j < $num - $i; $j++) {
            if ($array[$j] > $array[$j + 1]) {
                $temp = $array[$j];
                $array[$j] = $array[$j + 1];
                $array[$j + 1] = $temp;
            }
        }
    }

    return $array;
}
