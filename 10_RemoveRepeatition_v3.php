<?php
/**
 * 问题：对数组去重并从大到小排序，要求运行时间不超过1秒
 *
 * 第三种思路：和第二种思路一样，只是把冒泡排序换成快速排序，这是因为冒泡排序的时间复杂度太高了，使用快速排序的话会比较快
 *
 * 由于排序之后相同的数会紧挨在一起，所以只需要在输出的时候判断一下当前的数a[i]与前面的数a[i-1]是否相同，相同的话则说明这个数已经输出了不用再次输出
 */

// 需排序去重的数组
$array = [20, 40, 32, 67, 40, 20, 89, 300, 400, 15];

// 最终的结果数组
$result = [];

// 进行快速排序
QuickSort($array);

// 先存第一个数
$result[] = $array[0];

$count = count($array);
// 如果和前一个数相同的话则不输出，否则输出
for ($i = 1; $i < $count; $i++) {
    if ($array[$i] != $array[$i - 1]) {
        $result[] = $array[$i];
    }
}

print_r($result);

/**
 * 快速排序
 * @param array $arr
 */
function QuickSort(array &$arr)
{
    $left = 0;
    $right = count($arr) - 1;
    QSort($arr, $left, $right);
}

/**
 * @param $array
 * @param $left
 * @param $right
 */
function QSort(array &$array, $left, $right)
{
    if ($left >= $right) {
        return;
    }

    // 将$array一分为二，获取基准数的位置并回归
    $pivot = Partition($array, $left, $right);

    // 对基准数的左边进行递归排序
    QSort($array, $left, $pivot - 1);

    // 对基准数的右边进行递归排序
    QSort($array, $pivot + 1, $right);
}

/**
 * 选取数组当中的一个关键字作为基准数，使得它处于数组某个位置时，左边的值比它小，右边的值比它大，该关键字叫做枢轴/基准数，使枢轴/基准数记录到位，并返回其所在位置
 * @param array $array
 * @param $left int 左边第一个元素下标
 * @param $right int 右边（最后）第一个元素下标
 * @return int
 */
function Partition(array &$array, $left, $right)
{
    // 选取子数组第一个元素作为枢轴/基准数
    $pivot = $array[$left];

    // 记录第一个元素的下标
    $first = $left;

    while ($left < $right) {
        // $j从最右边往左边开始找，大于基准数$temp的话则继续往左边走
        while ($left < $right && $array[$right] >= $pivot) {
            $right--;
        }

        // $i从左边往右边找，小于基准数$temp的话则继续往右边走
        while ($left < $right && $array[$left] <= $pivot) {
            $left++;
        }

        // 当$i和$j没有相遇的时候则交换两者的位置
        if ($left < $right) {
            $t = $array[$left];
            $array[$left] = $array[$right];
            $array[$right] = $t;
        }
    }

    // 将基准数归位
    $array[$first] = $array[$left];
    $array[$left] = $pivot;

    // 返回$right也行，毕竟最后$left和$right都是停留在pivot下标处
    return $left;
}
