<?php
/**
 * 问题：求两个自然数的最大公约数。
 *
 * 分析：这个是基础的数学问题，最大公约数指两个数字公共的约数中最大的，例如数字6的约数有1、2、3、6，数字9的约数有1、3、9，则数字6和数字9的公共约数有1和3，其中3是最大的公约数。
 *
 * 第一种思路：从1开始循环，每次把符合要求(即同时是两个数字的约数)的值都存储起来，那么最后一个存储起来的就是最大的约数
 */

/**
 * 求两个自然数的最大公约数(暴力枚举法/穷举法)
 * @param int $a
 * @param int $b
 * @return bool|int
 */
function get_max_divisor($a, $b)
{
    if (!is_numeric($a) || !is_numeric($b)) {
        return false;
    }

    $min = $a > $b ? $b : $a;
    $max = $a > $b ? $a : $b;

    if ($max % $min === 0) {
        return $min;
    }

    $result = 1;
    for ($i = 1; $i <= $min / sqrt(2); $i++) {
        if ($min % $i === 0 && $max % $i === 0) {
            $result = $i;
        }
    }
    return $result;
}

$a = 36;
$b = 16;
$c = get_max_divisor($a, $b);
var_dump($c);
