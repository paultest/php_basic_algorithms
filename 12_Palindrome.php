<?php
/**
 * 判断一个字符是否是回文符，回文符即字符串从前往后读和从后往前读字符顺序是一致的
 *
 * 利用栈来判断，由于回文符是中间对称的，将回文符的前面一半入栈，然后出栈和后面一半一一对比匹配，都是一样的话表示是回文符
 */

$str = '12321';
$res = IsPalindrome($str);
var_dump($res);

/**
 * 判断是否是回文字符
 * @param $str
 * @return bool
 */
function IsPalindrome($str)
{
    // 读取字符的长度
    $len = strlen($str);

    // 计算中点位置
    $mid = intval($len / 2) - 1;

    // 栈的初始化
    $top = 0;
    $arr = [];

    // 先将中点之前的字符全部入栈
    for ($i = 0; $i <= $mid; $i++) {
        $arr[++$top] = $str[$i];
    }

    // 判断字符串的长度是奇数还是偶数，并找出需要进行字符匹配的起始下标
    if ($len % 2 === 0) {
        $next = $mid + 1;
    } else {
        $next = $mid + 2;
    }

    // 开始匹配，对比中点之前的字符和中点之后的字符是否一致
    for ($i = $next; $i <= $len - 1; $i++) {
        if ($str[$i] != $arr[$top]) {
            break;
        }
        $top--;
    }

    // 如果top为0，则表示栈内的所有字符都一一匹配，说明是回文符
    if ($top === 0) {
        return true;
    }

    return false;
}
