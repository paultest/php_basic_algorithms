<?php
/**
 * 斐波那契数列：1 1 2 3 5 8 13 21 34 55 …
 * 概念：
 *  前两个值都为1，该数列从第三位开始，每一位都是前两位的和
 *
 * 规律公式为：Fn = F(n-1) + F(n+1)
 *
 * 第一种思路：使用递归实现
 *
 * 这个是在第一种思路上面进行优化，由于在递归过程中，比如求Fibonacci(6)的时候呢，由于之前的递归已经求出了Fibonacci(5)和Fibonacci(4)了，所以可以没必要再重复算了
 *
 */

// 记录开始时间
$start_time = microtime(true);

$n = 30;
$array = [];
Fibonacci($n);
print_r($array);

// 记录结束时间
$end_time = microtime(true);

// 记录耗时时间
echo round($end_time - $start_time, 3) . ' 秒';

/**
 * 实现斐波那契数列
 * @param $n
 * @return bool
 */
function Fibonacci($n)
{
    global $array;

    if ($n < 1) {
        return false;
    }

    for ($i = 0; $i < $n; $i++) {
        $array[$i] = Fib($i);
    }
    return true;
}

/**
 * 获得斐波那契数列，第N位的数字
 * @param $n
 * @return int
 */
function Fib($n)
{
    global $array;

    // 如果已经获得第N位的数字的话，则返回
    if (isset($array[$n])) {
        return $array[$n];
    }

    // 如果是第一位和第二位的话，则返回1
    if ($n == 0 | $n == 1) {
        return 1;
    }

    return Fib($n - 1) + Fib($n - 2);
}