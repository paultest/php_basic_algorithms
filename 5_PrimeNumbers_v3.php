<?php
/**
 * 求素数问题：求出1000以内的所有素数，素数即质数，只能被1和本身整除的数，最小的质数是2。
 *
 * 实现思路：通过嵌套循环找出2到1000内所有的符合条件的数
 *
 * 再优化5_a_prime_number.php：由于每次判断的时候都会从2检查到i，把i/2变成根号i，因为根号i以上的数肯定不会被i整除
 */

for ($i = 2; $i <= 997; $i++) {
    $is_prime = true;
    for ($j = 2; $j <= sqrt($i); $j++) {
        if ($i !== $j && $i % $j === 0) {
            $is_prime = false;
            break;
        }
    }

    if ($is_prime === true) {
        echo $i . PHP_EOL;
    }
}