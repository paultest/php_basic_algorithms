<?php
/**
 * 寻找孤立数字：给定一个数组，数组内的数两两相同，只有一个数是孤立的，用最快的方式找出这个数。
 *
 * 分析：循环数组，判断第i个元素的值和其它位置的值是否相等，如果不存在相等的，那么这个数就是孤立数据
 *
 * 优化10_a_find_single_num.php：由于这样的嵌套循环判断复杂度是很高的，达到n的平方，可以使用异或(^)
 */

$array = [1, 2, 3, 2, 3, 1, 4, 7, 6, 4, 7];
$res = find_single_num($array);
echo $res;

/**
 * 从数组(数组内的数两两相同，只有一个数是孤立的)中寻找孤立的数字
 * @param array $array 数组
 * @return bool|int|mixed
 */
function find_single_num($array)
{
    if (!is_array($array)) {
        return false;
    }
    $single = 0;
    foreach ($array as $value) {
        $single = $single ^ $value;
    }
    return $single;
}