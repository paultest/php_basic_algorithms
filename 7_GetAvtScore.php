<?php
/**
 * 歌手打分问题：在歌唱比赛中，共有10位评委进行打分，在计算歌手得分时，去掉一个最高分，去掉一个最低分，然后剩余的8位评委的分数进行平均，就是该选手的最终得分。如果已知每个评委的评分，求该选手的得分。
 *
 * 分析：该题实际上涉及到求数组的最大值、最小值，以及求数组中所有元素的和，也是数组方便统计的用途体现。
 *
 * 实现思路：求出数组元素的最大值、最小值以及和，然后使用和减去最大值和最小值，然后除以8获得得分
 */

$score = array(90, 78, 90, 96, 67, 86, 78, 92, 79, 85);
$avg = (array_sum($score) - max($score) - min($score)) / 8;
echo $avg;
