<?php
/**
 * 问题：求两个自然数的最大公约数。
 *
 * 分析：这个是基础的数学问题，最大公约数指两个数字公共的约数中最大的，例如数字6的约数有1、2、3、6，数字9的约数有1、3、9，则数字6和数字9的公共约数有1和3，其中3是最大的公约数。
 *
 * 第二种思路：从两个数字中最小的数字开始循环，每次减1，那么第一次得到的公共约数就是所求的最大公约数
 */

/**
 * 求两个自然数的最大公约数(暴力枚举法/穷举法——优化)
 * 时间复杂度是O(min(a, b)))
 * @param int $a
 * @param int $b
 * @return bool|int
 */
function get_max_divisor($a, $b)
{
    if (!is_numeric($a) || !is_numeric($b)) {
        return false;
    }

    $min = $a > $b ? $b : $a;
    $result = 1;
    for ($i = $min; $i >= 1; $i--) {
        if ($a % $i === 0 && $b % $i === 0) {
            $result = $i;
            break;
        }
    }
    return $result;
}

$a = 55;
$b = 10;
$c = get_max_divisor($a, $b);
var_dump($c);
