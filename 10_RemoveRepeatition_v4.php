<?php
/**
 * 问题：对数组去重并从大到小排序，要求运行时间不超过1秒
 *
 * 第四种思路：使用PHP内置的数组去重函数和排序函数来做
 *
 * PHP内置的去重函数有：
 *  array_unique函数：移除数组中重复的元素，可以用来实现对数组里的元素去重，但是由于删掉数组中重复的元素会导致key不按顺序，所以还需要使用array_values来对key重新排序
 *
 *  array_flip函数：将数组中的键值和值进行反转，如果出现同一值，则最后一个键名将作为它的值，可以利用双重array_flip函数来去重，因为键值互换，原来重复的值会变为相同的键。然后再进行一次键值互换，把键和值换回来则可以完成去重，且效率要比array_unique函数高
 */

// 需排序去重的数组
$array = [20, 40, 32, 67, 40, 20, 89, 300, 400, 15];

// 最终的结果数组
$result = [];

// 数组去重 array_flip()函数：将数组中的键值和值进行反转，如果出现同一值，则最后一个键名将作为它的值  利用双重array_flip可以实现数组去重，且效率要比PHP另一个数组去重函数array_unique要高得多
$result = array_flip(array_flip($array));

// 排序
sort($result);

print_r($result);
