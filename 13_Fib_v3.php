<?php
/**
 * 斐波那契数列：1 1 2 3 5 8 13 21 34 55 …
 * 概念：
 *  前两个值都为1，该数列从第三位开始，每一位都是前两位的和
 *
 * 规律公式为：Fn = F(n-1) + F(n+1)
 *
 * 第二种思路：使用循环实现
 *
 * 相比第一种思路使用递归去实现，效率要高得多，一般来说，非递归的效率高于递归
 */

// 记录开始时间
$start_time = microtime(true);

$n = 30;
$res = Fibonacci($n);
print_r($res);

// 记录结束时间
$end_time = microtime(true);

// 记录耗时时间
echo round($end_time - $start_time, 3) . ' 秒';

/**
 * 实现斐波那契数列
 * @param $n
 * @return array|bool
 */
function Fibonacci($n)
{
    if ($n < 1) {
        return false;
    }

    $res = [1, 1];

    for ($i = 2; $i < $n; $i++) {
        $res[$i] = $res[$i - 1] + $res[$i - 2];
    }

    return $res;
}