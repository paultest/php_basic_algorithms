<?php
/**
 * 问题：对数组去重并从大到小排序，要求运行时间不超过1秒
 *
 * 第一种思路：先将数组去重，再从小到大进行排序
 * 排序的话如果数组里面的数字并不大的话，可以使用桶排序
 */

// 需排序去重的数组
$array = [20, 40, 32, 67, 40, 20, 89, 300, 400, 15];

// 最终的结果数组
$result = [];

// 数组的最大值
$max = max($array);

// 设置一个桶数组，长度为需要排序数组的最大值，并且初始化都为0
$bucket = array_fill(0, $max + 1, 0);

// 遍历需排序的数组，对桶数组的值赋值为1
foreach ($array as $value) {
    $bucket[$value] = 1;
}

// 从小到大输出桶数组的值
foreach ($bucket as $key => $value) {
    if ($value == 1) {
        $result[] = $key;
    }
}

print_r($result);
