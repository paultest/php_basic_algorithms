<?php
/**
 * 求素数问题：求出1000以内的所有素数，素数即质数，只能被1和本身整除的数，最小的质数是2。
 *
 * 实现思路：通过嵌套循环找出2到1000内所有的符合条件的数
 */

for ($i = 2; $i <= 997; $i++) {
    $is_prime = true;
    for ($j = 2; $j < $i; $j++) {
        if ($i !== $j && $i % $j === 0) {
            $is_prime = false;
            break;
        }
    }

    if ($is_prime === true) {
        echo $i . PHP_EOL;
    }
}