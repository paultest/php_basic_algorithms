<?php
/**
 * 需要对一串数字进行解密
 *
 * 解密规则：将第一个数删除，然后将第二个数放到末尾，接着第三个数删除并将第四个数放到末尾，再将第五个数删除，......，以此类推，直到剩下最后一个数，将最后一个数删除。按照刚才删除的顺序，将删除的数连起来就是解密后的数字了
 *
 * 思路：使用队列来模拟解密
 *
 * 队列：一种特殊的线性结构，只允许在列队的首部(head)进行删除操作，称为出队，而在队列的尾部(tail)进行插入操作，称为入队，当队列中没有元素时(即head=tail),称为空队列
 *
 * 队列遵循先进先出(First In First Out，FIFO)原则
 *
 * 如果要删除一个数的话，只需要head++即可，把需要增加的数放到队尾即array[tail]，然后tail++
 */

// 需解密的数字
$array = [6, 3, 1, 7, 5, 8, 9, 2, 4];

// 进行解密
$res = Decrypt($array);
var_dump($res);

/**
 * 解密
 * @param $array
 * @return array
 */
function Decrypt($array)
{
    $res = [];

    // 队首
    $head = 0;

    // 队尾的下一个位置
    $tail = count($array);

    // 当队列不为空(当队首和队尾重合的时候即为空)的时候执行循环
    while ($head < $tail) {
        // 把队首放到新数组里面
        $res[] = $array[$head];

        // 将队首出队
        $head++;

        // 如果队首和队尾的下一个位置重合的话，则说明队列为空了，则退出
        if ($head >= $tail) {
            break;
        }
        
        // 将新队首的数添加到队尾
        $array[$tail] = $array[$head];
        $tail++;

        // 再将队首出队
        $head++;
    }
    return $res;
}